<?php 
    session_start();
    if (isset($_SESSION['dash']) && !empty($_SESSION['dash'])) {
        $id = $_SESSION['dash'];
        $sql = $pdo->prepare("SELECT * FROM usuarios WHERE id = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0 ) {
            $info = $sql->fetch();
            
        } else {
            header("Location: login.php");
            exit;
        }
    }  else {
        header("Location: login.php");
        exit;
    }

?>