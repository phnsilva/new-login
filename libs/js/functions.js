let page = "home";
let pagePath = "./pages/";
let pageExt = ".html";
let url = callPage();

$('.nav-item > a').on('click', function(e) {
    e.preventDefault();
    console.log('hi');
    url = callPage($(e.target).attr('href'));
    drawPage();
});


function drawPage(){
    fetch(url)
        .then(function (response) {
            return response.text();
        })
        .then(function (html) {
            $('.target').html(html);
        })
        .catch(function (err) {
            console.log('Failed to fetch page: ', err);
        });
}

function callPage(pg) {
    if (pg) {
        return pagePath + pg + pageExt;    
    }
    else {
        return pagePath + page + pageExt;
    }
}