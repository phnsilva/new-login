<?php
    session_start();
    require "conexao.php"; //Importa a conexão para ser usada aqui

    //Verifica se o campo senha existe e se esta preenchido
    if (isset($_POST['email']) &&  !empty($_POST['email'])) { 
        $email = addslashes($_POST['email']); //Pega o senha do Formulário através do attr name
        $senha = md5(addslashes($_POST['senha']));

        $sql = $pdo->prepare("SELECT * FROM usuarios WHERE email = :email AND senha = :senha"); // Monta a Query do banco 
        $sql->bindValue(":email", $email);
        $sql->bindValue(":senha", $senha);
        $sql->execute();
        
        if ($sql->rowCount() > 0) { //metodo rowCount é do PDO e retorna quantos registros a query tem
            $dado = $sql->fetch(); //metodo Fetch pega o primeiro resultado da requisição e monta o array em $dado
            
            $_SESSION['dash'] = $dado['id'];
            
            header('Location: dash.html');

        } 
    } else {
        header("Location: login.html");
        exit;
    }
?>